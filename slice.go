package gojql

// GetSlice tries to get the value stored in the given store and key as a string slice
func (c *Client) GetStringSlice(store, key string) []string {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.([]string)
		if ok {
			return cast
		}
	}

	return nil
}

// GetSlice tries to get the value stored in the given store and key as an int64 slice
func (c *Client) GetInt64Slice(store, key string) []int64 {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.([]int64)
		if ok {
			return cast
		}
	}

	return nil
}

// GetSlice tries to get the value stored in the given store and key as a float64 slice
func (c *Client) GetFloat64Slice(store, key string) []float64 {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.([]float64)
		if ok {
			return cast
		}
	}

	return nil
}

// GetSlice tries to get the value stored in the given store and key as a bool slice
func (c *Client) GetBoolSlice(store, key string) []bool {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.([]bool)
		if ok {
			return cast
		}
	}

	return nil
}

// GetSlice tries to get the value stored in the given store and key as a byte slice
func (c *Client) GetByteSlice(store, key string) []byte {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.([]byte)
		if ok {
			return cast
		}
	}

	return nil
}
