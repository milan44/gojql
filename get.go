package gojql

// GetSlice tries to get the value stored in the given store and key as an interface slice
func (c *Client) GetSlice(store, key string) []interface{} {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.([]interface{})
		if ok {
			return cast
		}
	}

	return nil
}

// GetSlice tries to get the value stored in the given store and key as a string
func (c *Client) GetString(store, key string) string {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.(string)
		if ok {
			return cast
		}
	}

	return ""
}

// GetSlice tries to get the value stored in the given store and key as an int64
func (c *Client) GetInt64(store, key string) int64 {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.(int64)
		if ok {
			return cast
		}
	}

	return 0
}

// GetSlice tries to get the value stored in the given store and key as a bool
func (c *Client) GetBool(store, key string) bool {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.(bool)
		if ok {
			return cast
		}
	}

	return false
}

// GetSlice tries to get the value stored in the given store and key as a float64
func (c *Client) GetFloat64(store, key string) float64 {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.(float64)
		if ok {
			return cast
		}
	}

	return 0
}

// GetSlice tries to get the value stored in the given store and key as a byte
func (c *Client) GetByte(store, key string) byte {
	v := c.Get(store, key)
	if v != nil {
		cast, ok := v.(byte)
		if ok {
			return cast
		}
	}

	return 0
}
