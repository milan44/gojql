package gojql

import (
	"fmt"
	"os"
	"testing"
)

type test struct {
	A string
	B int64
	C float64
}

func TestClient(t *testing.T) {
	_ = os.Remove("./test.json")

	client, err := NewClient("./test.json")
	must(err, t)

	err = client.Set("test", "key", test{
		A: "asdf",
		B: 1234,
		C: 12.34,
	})
	must(err, t)

	tst := client.Get("test", "key").(test)

	assert(tst.A, "asdf", t)
	client.ForceClose()

	client2, err := NewClient("./test.json")
	must(err, t)

	tst2 := client.Get("test", "key").(test)

	err = client2.Set("test", "key2", "This is a test")
	must(err, t)

	assert(tst2.A, "asdf", t)
	client2.ForceClose()

	_ = os.Remove("./test.json")
}

func must(err error, t *testing.T) {
	if err != nil {
		t.Fatalf("An error occured: %s", err.Error())
	}
}

func assert(a, b string, t *testing.T) {
	if a != b {
		t.Fatalf("%s != %s", a, b)
	} else {
		fmt.Printf("%s == %s\n", a, b)
	}
}
