package gojql

import (
	"os"
	"sync"
)

type Client struct {
	storage      *os.File
	storageMutex sync.Mutex

	memory      map[string]map[string]interface{}
	memoryMutex sync.Mutex
}

// NewClient creates a new gojql client
func NewClient(storage string) (c *Client, err error) {
	c = &Client{
		memory: make(map[string]map[string]interface{}),
	}

	err = c.ReadIfExists(storage)
	if err != nil {
		c = nil
		return
	}

	err = c.Persist()
	if err != nil {
		c = nil
		return
	}

	return
}

// CreateStore ensures that a certain store exists
func (c *Client) CreateStore(store string) {
	if !c.StoreExists(store) {
		c.memoryMutex.Lock()
		c.memory[store] = make(map[string]interface{})
		c.memoryMutex.Unlock()
	}
}

// SetSoft sets a value in memory but doesn't persist it
func (c *Client) SetSoft(store, key string, value interface{}) {
	c.CreateStore(store)

	c.memoryMutex.Lock()
	c.memory[store][key] = value
	c.memoryMutex.Unlock()
}

// Set calls SetSoft and then Persist
func (c *Client) Set(store, key string, value interface{}) error {
	c.SetSoft(store, key, value)
	return c.Persist()
}

// StoreExists returns if a certain store exists
func (c *Client) StoreExists(store string) bool {
	c.memoryMutex.Lock()
	_, ok := c.memory[store]
	c.memoryMutex.Unlock()

	return ok
}

// KeyExists returns if a certain key in a store exists
func (c *Client) KeyExists(store, key string) bool {
	if !c.StoreExists(store) {
		return false
	}

	c.memoryMutex.Lock()
	_, ok := c.memory[store][key]
	c.memoryMutex.Unlock()

	return ok
}

// Get returns the value stores at a certain store and key of nil if it doesn't exist
func (c *Client) Get(store, key string) interface{} {
	if !c.KeyExists(store, key) {
		return nil
	}

	c.memoryMutex.Lock()
	val, _ := c.memory[store][key]
	c.memoryMutex.Unlock()

	return val
}
