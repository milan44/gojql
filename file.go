package gojql

import (
	"encoding/json"
	"github.com/pkg/errors"
	"os"
)

// ReadIfExists reads the given json file into memory if it exists
func (c *Client) ReadIfExists(path string) error {
	_, err := os.Stat(path)

	if err == nil {
		return c.Read(path)
	} else {
		c.memoryMutex.Lock()
		c.memory = make(map[string]map[string]interface{})
		c.memoryMutex.Unlock()

		err := c.openFile(path)
		if err != nil {
			return err
		}
	}

	return nil
}

// Read reads the given json file into memory
func (c *Client) Read(path string) error {
	err := c.openFile(path)
	if err != nil {
		return err
	}

	c.storageMutex.Lock()
	d := json.NewDecoder(c.storage)

	c.memoryMutex.Lock()
	err = d.Decode(&c.memory)
	c.memoryMutex.Unlock()

	c.storageMutex.Unlock()

	if err != nil {
		return err
	}

	return nil
}

// Persist encodes and writes the current memory to the json file
func (c *Client) Persist() error {
	c.storageMutex.Lock()
	if c.storage == nil {
		c.storageMutex.Unlock()
		return errors.New("storage is already closed")
	}
	c.storageMutex.Unlock()

	c.memoryMutex.Lock()
	b, err := json.Marshal(c.memory)
	c.memoryMutex.Unlock()
	if err != nil {
		return err
	}

	c.storageMutex.Lock()
	n, err := c.storage.WriteAt(b, 0)
	if err != nil {
		c.storageMutex.Unlock()
		return err
	}

	err = c.storage.Truncate(int64(n))
	c.storageMutex.Unlock()

	return err
}

// Close Persists and then closes the file
func (c *Client) Close() (err error) {
	c.storageMutex.Lock()
	if c.storage == nil {
		c.storageMutex.Unlock()
		return errors.New("storage is already closed")
	}

	err = c.Persist()
	if err != nil {
		c.storageMutex.Unlock()
		return
	}

	err = c.storage.Close()
	c.storage = nil
	c.storageMutex.Unlock()

	return
}

// ForceClose Persists and then closes the file but ignores any errors
func (c *Client) ForceClose() {
	c.storageMutex.Lock()
	if c.storage != nil {
		c.storageMutex.Unlock()
		_ = c.Persist()

		c.storageMutex.Lock()
		_ = c.storage.Close()

		c.storage = nil
	}
	c.storageMutex.Unlock()
}

func (c *Client) openFile(path string) error {
	c.storageMutex.Lock()
	if c.storage == nil {
		f, err := os.OpenFile(path, os.O_CREATE|os.O_RDWR, 640)
		if err != nil {
			c.storageMutex.Unlock()
			return err
		}

		c.storage = f
	}
	c.storageMutex.Unlock()

	return nil
}
